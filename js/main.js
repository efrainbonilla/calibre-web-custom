
$(function() {
    $("#bookViewModal")
        .on("show.bs.modal", function(e) {
            var $modalBody = $(this).find(".modal-body");
            var iframe = $('<iframe id="viewpdf"></iframe>');
            $modalBody.append(iframe);
            iframe.attr('src', e.relatedTarget.href);
            iframe.attr('width', '100%');
            iframe.attr('height', $( window ).height() + 'px');
        })
        .on("hidden.bs.modal", function() {
            $(this).find(".modal-body").html("");
        });

});
