#!/bin/bash
#
CALIBRE_DEST="/home/biblioteca"

CALIBREAPP_DEST="/opt/calibre"
CALIBREAPP_EXE="/opt/calibre/bin/calibre"
CALIBREAPP_NAME="Calibre"
CALIBREAPP_LIBRARY_NAME="calibre"
CALIBREAPP_LIBRARY_DEST="$CALIBRE_DEST/$CALIBREAPP_LIBRARY_NAME"


CALIBREWEB_NAME="calibre-web"
CALIBREWEB_DEST="$CALIBRE_DEST/$CALIBREWEB_NAME"
CALIBREWEB_GIT="https://github.com/janeczku/calibre-web.git"

sudo -v
if [[ $? != 0 ]]; then
	echo "Habilitar autenticación para obtener los privilegios necesarios. Finalizando..."
	echo -n "Presione <Intro> para continuar"
	read WER
	exit $?
fi

if [[ ! -d "$CALIBRE_DEST" ]]; then
	mkdir -p "$CALIBRE_DEST"
	sudo chmod 777 -R "$CALIBRE_DEST"
fi

if [[ ! -d "$CALIBREAPP_LIBRARY_DEST" ]]; then
	echo "Creando biblioteca calibre ..."
	mkdir -p "$CALIBREAPP_LIBRARY_DEST"
	sudo chmod 777 -R "$CALIBREAPP_LIBRARY_DEST"
fi



install_calibre_app() {
	if [[ -f "$CALIBREAPP_EXE" ]]; then
		read -p "Ya existe una versión de $CALIBREAPP_NAME instalado quiere remplazarlo [y/n] ?"
		if [[ "$REPLY" == "y" ]]; then
			echo " Remplazando $CALIBREAPP_NAME ..."
			sudo calibre-uninstall
		else
			echo "Instalación cancelado"
			return 1
		fi
	fi
	echo "Instalando $CALIBREAPP_NAME ..."
	sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.py | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"
	
	echo
	echo "Finalizado ..."
	echo


	sudo -u "$(basename $HOME)" calibre &>/dev/null & disown %% && echo -e "...Ok.\n"

}

install_calibre_web() {
	echo
	echo " - Preparando instalación calibre-web ..."
	echo
	echo ""
	echo " - Instalando dependencias python-pip python3-pip ..."
	sudo apt install -y python-pip python3-pip

	echo " - Instalando dependencias git ..."
	sudo apt install -y git

	if  which pip > /dev/null; then
			echo "pip esta instalado..."
	else
		echo "pip no esta instalado. intente de nuevo."
		return 1;
	fi

	if  which git > /dev/null; then
			echo "git esta instalado..."
	else
		echo "git no esta instalado. intente de nuevo."
		return 1;
	fi

	if [[ -f "$CALIBREWEB_DEST/cps.py" ]]; then
		echo
		read -p "Ya existe una versión de $CALIBREWEB_NAME instalado quiere remplazarlo [y/n] ?"
		if [[ "$REPLY" == "y" ]]; then
			echo " Remplazando $CALIBREWEB_NAME ..."
			cp  $CALIBREWEB_DEST "${CALIBREWEB_DEST}_old"
			rm -rf $CALIBREWEB_DEST
		else
			echo "Instalación cancelado"
			return 1
		fi
	fi

	git clone $CALIBREWEB_GIT "$CALIBREWEB_DEST"

	sudo chmod 777 -R "$CALIBREWEB_DEST"

	echo "Instalando dependencias $CALIBREWEB_NAME ..."
	cd "$CALIBREWEB_DEST" && pip install --target vendor -r requirements.txt && pip install --upgrade pip

	sleep 5

	mkdir -p "$HOME/.config/autostart"

	echo "Integrando $CALIBREWEB_NAME en [$HOME/.config/autostart/$CALIBREWEB_NAME.sh.desktop]"

	echo "[Desktop Entry]
	Type=Application
	Name=calibre-web
	Exec=python $CALIBREWEB_DEST/cps.py" | sudo tee "$HOME/.config/autostart/$CALIBREWEB_NAME.sh.desktop"
		sudo chmod +x "$HOME/.config/autostart/$CALIBREWEB_NAME.sh.desktop" && echo -e "...Ok.\n"

	echo "Iniciando servidor $CALIBREWEB_NAME ..."
	sudo -u "$(basename $HOME)" "python" "$CALIBREWEB_DEST/cps.py" &>/dev/null & disown %% && echo -e "...Ok.\n"

	sleep 3

	echo "Abriendo en navegador $CALIBREWEB_NAME ..."
	sudo -u "$(basename $HOME)" x-www-browser "http://localhost:8083" &>/dev/null & disown %% && echo -e "...Ok.\n"

	echo "Creando servicio [cps.service] en [/etc/systemd/system/cps.service]"
	echo "Description=Calibre-web

[Service]
Type=simple
User=$(basename $HOME)
ExecStart=python $CALIBREWEB_DEST/cps.py

[Install]
WantedBy=multi-user.target" | sudo tee "/etc/systemd/system/cps.service"

	sudo chmod +x "/etc/systemd/system/cps.service" && echo -e "...Ok.\n"

	echo
	echo "Finalizado ..."
	echo
}

install_calibre_web_custom() {
	if [[ ! -d "$CALIBREWEB_DEST" ]]; then
		echo "Error, $CALIBREWEB_NAME no esta instalado."
		install_calibre_web
	fi

	if [[ ! -d "$CALIBREWEB_DEST" ]]; then
		echo "Error, $CALIBREWEB_NAME no esta instalado."
		return 1;
	fi

	STATIC_DEST="$CALIBREWEB_DEST/cps/static"
	CUSTOM_DEST="$STATIC_DEST/custom"

	if [[ ! -d "$STATIC_DEST" ]]; then
		echo "Error, directorio $STATIC_DEST no existe."
		return 1
	fi

	if [[ -d "$CUSTOM_DEST" ]]; then
		echo
		read -p "Ya existe una versión de $CUSTOM_DEST parchado sobreescribirlo [y/n] ?"
		if [[ "$REPLY" == "y" ]]; then
			rm -rf $CUSTOM_DEST
		else
			echo "Instalación cancelado"
			return 1
		fi
	fi

	git clone  https://gitlab.com/efrainbonilla/calibre-web-custom.git "$CUSTOM_DEST"

	# inicio codigo personalizado


	READPDF="$CALIBREWEB_DEST/cps/templates/readpdf.html"
	VIEWER_CSS_BEFORE="<link rel=\"stylesheet\" href=\"{{ url_for('static', filename='css/libs/viewer.css') }}\">"
	VIEWER_CSS_AFTER="<link rel=\"stylesheet\" href=\"{{ url_for('static', filename='custom/css/libs/viewer.css') }}\">"
	if grep -rl "$VIEWER_CSS_BEFORE" "$READPDF"
	then
		if grep -rl "$VIEWER_CSS_AFTER" "$READPDF"
		then
			echo "$VIEWER_CSS_AFTER ya esta en [$READPDF]"
		else
	    	max=0
			MY=`sed -n "/viewer.css/=" "$READPDF"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} + 1)
				sudo sed -i "${max}i $VIEWER_CSS_AFTER" "$READPDF"
			fi
		fi
	fi

	READPDF="$CALIBREWEB_DEST/cps/static/js/libs/viewer.js"
	VIEWER_CSS_BEFORE="event.keyCode === 80 && (event.ctrlKey || event.metaKey) && !event.altKey && (!event.shiftKey || window.chrome || window.opera)"
	VIEWER_CSS_AFTER="return false; //disabledprint"
	if grep -rl "$VIEWER_CSS_BEFORE" "$READPDF"
	then
		if grep -rl "$VIEWER_CSS_AFTER" "$READPDF"
		then
			echo "$VIEWER_CSS_AFTER ya esta en [$READPDF]"
		else
	    	max=0
			MY=`sed -n "/window.opera/=" "$READPDF"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} + 1)
				sudo sed -i "${max}i $VIEWER_CSS_AFTER" "$READPDF"
			fi
		fi
	fi


	READPDF="$CALIBREWEB_DEST/cps/templates/readpdf.html"
	SHORTCUT_JS_BEFORE="<script src=\"{{ url_for('static', filename='js/libs/pdf.js') }}\"></script>"
	SHORTCUT_JS_AFTER="<script src=\"{{ url_for('static', filename='custom/js/shortcut.js') }}\"></script>"
	if grep -rl "$SHORTCUT_JS_BEFORE" "$READPDF"
	then
		if grep -rl "$SHORTCUT_JS_AFTER" "$READPDF"
		then
			echo "$SHORTCUT_JS_AFTER ya esta en [$READPDF]"
		else
	    	max=0
			MY=`sed -n "/pdf.js/=" "$READPDF"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} + 1)
				sudo sed -i "${max}i $SHORTCUT_JS_AFTER" "$READPDF"
			fi
		fi
	fi

	PRINT_BEFORE="<div class=\"buttonRow\">"
	PRINT_AFTER="<div id=\"printServiceOverlay\" class=\"container hidden\">"
	PRINT_AFTER_="<div id=\"printServiceOverlay\" class=\"container hidden\"> <div class=\"dialog\"> <div class=\"row\"> <span data-l10n-id=\"print_progress_message\">Preparing document for printing…</span> </div> <div class=\"row\"> <progress value=\"0\" max=\"100\"></progress> <span data-l10n-id=\"print_progress_percent\" data-l10n-args='{ \"progress\": 0 }' class=\"relative-progress\">0%</span> </div> <div class=\"buttonRow\"> <button id=\"printCancel\" class=\"overlayButton\"><span data-l10n-id=\"print_progress_close\">Cancel</span></button> </div> </div> </div> <div id=\"chromeFileAccessOverlay\" class=\"container hidden\"> <div class=\"dialog\"> <div class=\"row\"> <p id=\"chrome-pdfjs-logo-bg\" style=\"display: block; padding-left: 60px; min-height: 48px; background-size: 48px; background-repeat: no-repeat; font-size: 14px; line-height: 1.8em; word-break: break-all;\"> Click on \"<span id=\"chrome-file-access-label\">Allow access to file URLs</span>\"at <a id=\"chrome-link-to-extensions-page\">chrome://extensions</a> <br> to view <span id=\"chrome-url-of-local-file\">this PDF file.</span> </p> </div> </div> </div>"

	if grep -rl "$PRINT_BEFORE" "$READPDF"
	then
		if grep -rl "$PRINT_AFTER" "$READPDF"
		then
			echo "$PRINT_AFTER ya esta en [$READPDF]"
		else
	    	max=0
			MY=`sed -n "/documentPropertiesClose/=" "$READPDF"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done
			echo "$max"

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} + 4)
				sudo sed -i "${max}i $PRINT_AFTER_" "$READPDF"
			fi
		fi
	fi

	BTN_PRINT_BEFORE="<link rel=\"stylesheet\" href=\"{{ url_for('static', filename='css/libs/viewer.css') }}\">"
	BTN_PRINT_AFTER="{% if (g.user.role_download() != true) %} <style> #print {display:none !important; }</style> {% endif %}"
	BTN_PRINT_AFTER_="$BTN_PRINT_AFTER"

	if grep -rl "$BTN_PRINT_BEFORE" "$READPDF"
	then
		if grep -rl "$BTN_PRINT_AFTER" "$READPDF"
		then
			echo "$BTN_PRINT_AFTER ya esta en [$READPDF]"
		else
	    	max=0
			MY=`sed -n "/viewer.css/=" "$READPDF"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done
			echo "$max"

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} + 1)
				sudo sed -i "${max}i $BTN_PRINT_AFTER_" "$READPDF"
			fi
		fi
	fi



	READPDF="$CALIBREWEB_DEST/cps/templates/layout.html"
	SHORTCUT_JS_BEFORE="<script src=\"{{ url_for('static', filename='js/main.js') }}\"></script>"
	SHORTCUT_JS_AFTER="<script src=\"{{ url_for('static', filename='custom/js/shortcut.js') }}\"></script>"
	if grep -rl "$SHORTCUT_JS_BEFORE" "$READPDF"
	then
		if grep -rl "$SHORTCUT_JS_AFTER" "$READPDF"
		then
			echo "$SHORTCUT_JS_AFTER ya esta en [$READPDF]"
		else
	    	max=0
			MY=`sed -n "/main.js/=" "$READPDF"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} + 1)
				sudo sed -i "${max}i $SHORTCUT_JS_AFTER" "$READPDF"
			fi
		fi
	fi

	LAYOUT="$CALIBREWEB_DEST/cps/templates/layout.html"
	STYLE_CSS_BEFORE="<link href=\"{{ url_for('static', filename='css/style.css') }}\" rel=\"stylesheet\" media=\"screen\">"
	STYLE_CSS_AFTER="<link href=\"{{ url_for('static', filename='custom/css/style.css') }}\" rel=\"stylesheet\" media=\"screen\">"
	if grep -rl "$STYLE_CSS_BEFORE" "$LAYOUT"
	then
		if grep -rl "$STYLE_CSS_AFTER" "$LAYOUT"
		then
			echo "$STYLE_CSS_AFTER ya esta en [$LAYOUT]"
		else
	    	max=0
			MY=`sed -n "/style.css/=" "$LAYOUT"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} + 1)
				sudo sed -i "${max}i $STYLE_CSS_AFTER" "$LAYOUT"
			fi
		fi
	fi

	LAYOUT="$CALIBREWEB_DEST/cps/templates/layout.html"
	MAIN_JS_BEFORE="<script src=\"{{ url_for('static', filename='js/main.js') }}\"></script>"
	MAIN_JS_AFTER="<script src=\"{{ url_for('static', filename='custom/js/main.js') }}\"></script>"
	if grep -rl "$MAIN_JS_BEFORE" "$LAYOUT"
	then
		if grep -rl "$MAIN_JS_AFTER" "$LAYOUT"
		then
			echo "$MAIN_JS_AFTER ya esta en [$LAYOUT]"
		else
	    	max=0
			MY=`sed -n "/main.js/=" "$LAYOUT"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} + 1)
				sudo sed -i "${max}i $MAIN_JS_AFTER" "$LAYOUT"
			fi
		fi
	fi



	LAYOUT="$CALIBREWEB_DEST/cps/templates/layout.html"
	MODAL_BLOCK_BEFORE="{% block modal %}{% endblock %}"
	MODAL_BLOCK_AFTER="<div class=\"modal fade\" id=\"bookViewModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"bookViewModalLabel\">"
	MODAL_BLOCK_AFTER_="<div class=\"modal fade\" id=\"bookViewModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"bookViewModalLabel\"> <div class=\"modal-dialog\" style=\"width:100%\" role=\"document\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button> <h4 class=\"modal-title\" id=\"bookViewModalLabel\">{{_('Book Details')}}</h4> </div> <div class=\"modal-body\" style=\"padding:0px;\"></div> <div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">{{_('Close')}}</button> </div> </div> </div> </div>"

	if grep -rl "$MODAL_BLOCK_BEFORE" "$LAYOUT"
	then
		if grep -rl "$MODAL_BLOCK_AFTER" "$LAYOUT"
		then
			echo "$MODAL_BLOCK_AFTER ya esta en [$LAYOUT]"
		else
	    	max=0
			MY=`sed -n "/block modal/=" "$LAYOUT"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done
			echo "$max"

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} )
				sudo sed -i "${max}i $MODAL_BLOCK_AFTER_" "$LAYOUT"
			fi
		fi
	fi


	DETAIL="$CALIBREWEB_DEST/cps/templates/detail.html"
	MODAL_JS_BEFORE="format.format|lower == 'epub'"
	MODAL_JS_AFTER="{% if format.format|lower == 'pdf' %}"
	MODAL_JS_AFTER_="{% if format.format|lower == 'pdf' %}<li><a href=\"{{ url_for('read_book', book_id=entry.id, book_format=format.format|lower) }}\" data-toggle=\"modal\" data-target=\"#bookViewModal\" data-remote=\"false\">{{format.format}}</a></li> {% else %} <li><a target=\"_blank\" href=\"{{ url_for('read_book', book_id=entry.id, book_format=format.format|lower) }}\">{{format.format}}</a></li> {% endif %}"
	MODAL_JS_BEFORE_="<li><a target=\"_blank\" href=\"{{ url_for('read_book', book_id=entry.id, book_format=format.format|lower) }}\">{{format.format}}</a></li>"
	if grep -rl "$MODAL_JS_BEFORE" "$DETAIL"
	then
		if grep -rl "$MODAL_JS_AFTER" "$DETAIL"
		then
			echo "$MODAL_JS_AFTER ya esta en [$DETAIL]"
		else
	    	max=0
			MY=`sed -n "/read_book/=" "$DETAIL"`;
			for m in ${MY[*]}; do
			    if [[ $m -gt  $max ]];
			    	then max=$m
			    fi
			done

			echo "$max"

			if [[ $max -gt 0 ]]; then
				max=$(expr ${max} )
				sudo sed -i "s#$MODAL_JS_BEFORE_*##" "$DETAIL" && echo -e "...Ok.\n"
				sudo sed -i "${max}i $MODAL_JS_AFTER_" "$DETAIL"
			fi
		fi
	fi

	# fin

	# soporte lenguaje español
	read -p "Esta seguro de sobreescribir archivos de traducción [es] [y/n] ?"
	if [[ "$REPLY" == "y" ]]; then
		rm -rf "$CALIBREWEB_DEST/cps/translations/es/*"
		cp -r "$CALIBREWEB_DEST/cps/static/custom/translations/es" "$CALIBREWEB_DEST/cps/translations"
		chmod 777 -R "$CALIBREWEB_DEST/cps/translations"
	fi
	# echo
	# echo "Para continuar con la instalación acontinuación se abrira una pagina web"
	# echo "luego  seguir la secuencia de pasos. desde el paso 3 en adelante."
	# sleep 5

	# sudo -u "$(basename $HOME)" x-www-browser "https://gitlab.com/efrainbonilla/calibre-web-custom" &>/dev/null & disown %% && echo -e "...Ok.\n"
}


echo
read -p "Instalar Application Calibre [y/n] ?"
if [[ "$REPLY" == "y" ]]; then
	install_calibre_app
fi

read -p "Instalar Application Calibre web [y/n] ?"
if [[ "$REPLY" == "y" ]]; then
	install_calibre_web
fi

read -p "Instalar Application Calibre web custom [y/n] ?"
if [[ "$REPLY" == "y" ]]; then
	install_calibre_web_custom
fi


echo
echo "Finalizado"
echo

exit 0