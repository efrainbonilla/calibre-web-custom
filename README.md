

# CALIBRE WEB

### Parche para ocultar botones de descarga imprimir, agregado visualización de pdf en modal

###### - Paso 1
---
Clonar el repositorio calibre-web
```
git clone https://github.com/janeczku/calibre-web.git
```
###### - Paso 2
---
Ingresar a la carpeta ```calibre-web/cps/static``` y clonar el repositorio
```
git clone  https://github.com/efrainbonilla/calibre-web-custom.git custom
```
###### - Paso 3
---
Agregar en el archivo ``` calibre-web/cps/templates/readpdf.html ```

despues del siguiente codigo
```
<link rel="stylesheet" href="{{ url_for('static', filename='css/libs/viewer.css') }}">
```

agregar el siguiente codigo
```
<link rel="stylesheet" href="{{ url_for('static', filename='custom/css/libs/viewer.css') }}">
```
###### - Paso 4
---
Agregar en el archivo ``` calibre-web/cps/templates/layout.html ```

despues del siguiente codigo
```
<script src="{{ url_for('static', filename='js/main.js') }}"></script>
```

agregar el siguiente codigo
```
<script src="{{ url_for('static', filename='custom/js/main.js') }}"></script>
```

###### - Paso 5
---
Agregar en el archivo ``` calibre-web/cps/templates/detail.html ```

despues del siguiente codigo
```
{%if format.format|lower == 'epub' or format.format|lower == 'txt' or format.format|lower == 'pdf' or format.format|lower == 'cbr' or format.format|lower == 'cbt' or format.format|lower == 'cbz' %}
```

susituir la linea
```
<li><a target="_blank" href="{{ url_for('read_book', book_id=entry.id, book_format=format.format|lower) }}">{{format.format}}</a></li>
```

por el siguiente codigo
```
{% if format.format|lower == 'pdf' %}
<li><a href="{{ url_for('read_book', book_id=entry.id, book_format=format.format|lower) }}" data-toggle="modal" data-target="#bookViewModal" data-remote="false">{{format.format}}</a></li>
{% else %}
<li><a target="_blank" href="{{ url_for('read_book', book_id=entry.id, book_format=format.format|lower) }}">{{format.format}}</a></li>
{% endif %}
```

###### - Paso 6
---
Agregar en el archivo
``` calibre-web/cps/templates/layout.html ```


antes del siguiente codigo
```
{% block modal %}{% endblock %}
```

agregar el siguiente codigo
```
 <div class="modal fade" id="bookViewModal" tabindex="-1" role="dialog" aria-labelledby="bookViewModalLabel">
 <div class="modal-dialog" style="width:100%" role="document">
     <div class="modal-content">
         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <h4 class="modal-title" id="bookViewModalLabel">{{_('Book Details')}}</h4>
         </div>
         <div class="modal-body">...</div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">{{_('Close')}}</button>
         </div>
     </div>
 </div>
 </div>
```
